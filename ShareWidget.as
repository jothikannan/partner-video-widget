﻿package 
{
	import adobe.utils.ProductManager;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import DottedLine;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.display.StageAlign
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign
	import flash.events.MouseEvent;
	import flash.net.navigateToURL;
	import flash.system.Security
	import flash.system.SecurityDomain;
	import flash.ui.Mouse;
	import flash.desktop.Clipboard;
	import flash.desktop.ClipboardFormats
	import flash.utils.setInterval;
	import flash.utils.clearInterval;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.filters.BevelFilter;
	import flash.external.ExternalInterface;
	
	
	/**
	 * ...
	 * @author Ari
	 */
	
	
	public class  ShareWidget extends MovieClip 
	{
		private var _width:int = 445;
		private var _height:int = 249;
		
		public var ID:String = "Share";
		private var ClientCode:String = "icue";
		private var bookmarkCode:String = "iCue";
		private var Title:String;
		private var Asset_ID:String;
		private var BookMarkURL:String;
		
		var line1:DottedLine;
		var line2:DottedLine;
		var line3:DottedLine;
		
		var line1Y:int = 25;
		var line2Y:int = 130;
		var line3Y:int = 220;
		
		private var xml:XML;
		private var image_url:String = "http://s7.addthis.com/services/";
		private var addThis_array:Array;
		private var bevel:BevelFilter;
		private var cueCard;
		private var xmlLoaded:Boolean = false;
		private var xmlLoading:Boolean = false;
		
		
		private var copying:Boolean = false;
		
		
		public function Share() {
			
			Security.allowDomain("*");
			Security.allowInsecureDomain("*");
			trace("~Share, updated Jan 7th");
			//this.stage.align = StageAlign.TOP_LEFT;
			
			copy_btn.label_txt.text = "COPY URL";
			close_btn.label_txt.text = "CLOSE";
			line1 = new DottedLine(418);
			line1.y = line1Y;
			line1.x = 5
			
			line2 = new DottedLine(418);
			line2.y = line2Y;
			line2.x = 5;
			
			line3 = new DottedLine(418);
			line3.y = line3Y;
			line3.x = 5;
			
			addChild(line1);
			addChild(line2);
			addChild(line3);
			//this.stage.addEventListener(Event.RESIZE, stageResize);
			addThis_array = new Array();
			
			bevel = new BevelFilter(-1.3,45,0x999999,.52,0x000000,.74,2,2,1,3);
			eventSetUp();
			
			Colors.changeTint(copy_btn.bg, 0x212328, 1);
			Colors.changeTint(close_btn.bg, 0x212328, 1);
		}
		private function eventSetUp() {
			// CLOSE BTN
			close_btn.buttonMode = true;
			close_btn.useHandCursor = true;
			close_btn.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
				
				dispatchEvent(new Event("CLOSE"));
			});
			close_btn.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				Colors.changeTint(close_btn.bg, 0xc8234a, 1);
				close_btn.filters = [bevel];
			});
			close_btn.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent) {
				Colors.changeTint(close_btn.bg, 0x212328, 1);
				close_btn.filters = [];
			});
			// COPY BTN
			copy_btn.buttonMode = true;
			copy_btn.useHandCursor = true;
			var this_mc:Share = this;
			copy_btn.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
				
				
				Clipboard.generalClipboard.setData(ClipboardFormats.TEXT_FORMAT, url_text.text);
				copy_btn.label_txt.text = "COPIED"
				Colors.changeTint(copy_btn.bg, 0x00FF00, 1);
				copying = true;
				
				
				var changeBackTimer:Timer = new Timer(3500, 1);
				changeBackTimer.addEventListener(TimerEvent.TIMER_COMPLETE, changeTextBackToCopy);
				changeBackTimer.start();
				this_mc.stage.focus = url_text;
				this_mc.url_text.setSelection(0, this_mc.url_text.length);
			});
			copy_btn.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				if(!copying){
					Colors.changeTint(copy_btn.bg, 0xF36F21, 1);
					copy_btn.filters = [bevel];
				}
			});
			copy_btn.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent) {
				if(!copying){
					Colors.changeTint(copy_btn.bg, 0x212328, 1);
					copy_btn.filters = [];
				}
			});
		}
		private function changeTextBackToCopy(e:TimerEvent) {
			copy_btn.label_txt.text = "COPY URL";
			Colors.changeTint(copy_btn.bg, 0x212328, 1);
			copying = false;
		}
		private function stageResize(e:Event) {
			
			arrangeContent(this.stage.stageWidth, this.stage.stageHeight);
		}
		
		public function getWidth():int {
			return _width;
		}
		public function getHeight():int {
			return _height;
		}
		public function init(_cueCard):void {
			//trace("------------ INIT SHARE -------------")
			Title = _cueCard.metaData.title;
			//trace("~Title: " + Title);
			Asset_ID = _cueCard.metaData.Asset_ID;
			BookMarkURL = _cueCard.metaData.bookMarkLink;
			var txt:TextField;
			
			url_text.wordWrap = true;
			url_text.text = BookMarkURL;
			url_text.wordWrap = true;
			cueCard = _cueCard;
			if (xmlLoaded == false && xmlLoading == false) {
				xmlLoading = true;
				loadXML();
			} 
			if (xmlLoaded == true && xmlLoading == false) {
				dispatchEvent(new Event("READY"));
			}
			
		}
		
		public function arrangeContent(bg_width:Number, bg_height:Number) {
			
			if (line1) {
				this.removeChild(line1);
			}
			if (line2) {
				this.removeChild(line2);
			}
			if (line3) {
				this.removeChild(line3);
			}
			line1 = new DottedLine(bg_width-30);
			line1.x = 5;
			line1.y = line1Y;
			line2 = new DottedLine(bg_width-30);
			line2.x = 5;
			line2.y = line2Y;
			line3 = new DottedLine(bg_width-30);
			line3.x = 5;
			line3.y = line3Y;
			addChild(line1);
			addChild(line2);
			addChild(line3);
			close_btn.x = bg_width - 77;
			copy_btn.x = bg_width - 77;
			for (var index:Number = 0 ; index < addThis_array.length ; index++) {
				var button:Sprite = addThis_array[index];
				var prevButton:Sprite = addThis_array[index - 1];
				if (prevButton) {
					if (prevButton.x + 90 + 90 < bg_width){
						button.x = prevButton.x + 90;
						button.y = prevButton.y;
					}else {
						button.x = 25;
						button.y = prevButton.y + 20
					}
				}else {
					button.x = 25;
					button.y = 52;
				}
			}
			
			// instructions_txt
			instructions_txt.width = bg_width - 92;
			url_text.width = bg_width - 140;
			url_bg.width = bg_width - 140;
		}
		
		// XML HANDLING
		private function loadXML() {
			trace("~SHARE cueCard.folderDirectory: " + cueCard.folderDirectory);
			var request:URLRequest = new URLRequest(cueCard.folderDirectory + "extensions/addThis.xml");
			var xmlLoader:URLLoader = new URLLoader();
			xmlLoader.addEventListener(Event.COMPLETE, handleXML);
			xmlLoader.addEventListener(IOErrorEvent.IO_ERROR, function(e:Event) {
					xmlLoading = false;
			});
			xmlLoader.load(request);

		}
		private function handleXML(e:Event):void {
			
			xmlLoaded = true;
			xmlLoading = false;
			xml = new XML(e.target.data);
			xml.ignoreWhitespace = true; 
			//trace(xml);
			var serv_array:XMLList = xml.Service;
			
			for(var index:Number = 0 ; index < serv_array.length(); index++){
				
				var addThis_btn:Sprite = createButton(serv_array[index]);
				
				addThis_array.push(addThis_btn);
				this.addChild(addThis_btn);
				//addThis_btn.x = 19 * index;
				
			}
			arrangeContent(_width, _height);
			dispatchEvent(new Event("READY"));
		}
		
		private function createButton(_info:XML):Sprite {
			var addThis_btn:DynamicSprite = new DynamicSprite();
			
			addThis_btn._info = _info;
			var loader:Loader = new Loader();
			var request:URLRequest = new URLRequest(image_url + String(_info.ThumbNail));
			loader.load(request);
			addThis_btn.addChild(loader);
			
			
			var label:GENERIC_BUTTON = new GENERIC_BUTTON();
			label.x = 18;
			label.hit_btn.width += 18;
			label.hit_btn.x -= 18;
			addThis_btn.addChild(label);
			
			addThis_btn.fmt = new TextFormat();
			addThis_btn.fmt.align = TextFormatAlign.LEFT;
			addThis_btn.fmt.color = 0x000000
			
			label.label_txt.text = _info.Label;
			label.label_txt.setTextFormat(addThis_btn.fmt);
			Colors.changeTint(label.bg, 0x212328, 1);
			label.bg.alpha = 0;
			
			label.hit_btn.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				
				e.currentTarget.parent.parent.fmt.color = 0xFFFFFF
				e.currentTarget.parent.label_txt.setTextFormat(e.currentTarget.parent.parent.fmt);
				e.currentTarget.parent.bg.alpha = 1;
			});
			label.hit_btn.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent) {
				e.currentTarget.parent.parent.fmt.color = 0x000000
				e.currentTarget.parent.label_txt.setTextFormat(e.currentTarget.parent.parent.fmt);
				e.currentTarget.parent.bg.alpha = 0;
			});
			label.hit_btn.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
				
				connectToService(e.currentTarget.parent.parent._info.ServiceCode);
				
			});
			
			return addThis_btn;
		}
		
		private function connectToService(_serviceCode:String) {
			if (_serviceCode == "favorites") {
			//	ExternalInterface.call("window.external.AddFavorite(" + escape(BookMarkURL) + ", " + String(Title) + ")"); // does not work!
				//return;
			}
			
			cueCard.omniture.trackEvent("event33", "Shaired With AddThis");
			var _url:String = "http://www.addthis.com/bookmark.php?pub=" + ClientCode + "&s=" + _serviceCode + "&url=" + escape(BookMarkURL) + "&title=" + String(Title);
			
			try {
				var request:URLRequest = new URLRequest(_url);
			
			
				navigateToURL(request,"_blank"); 
			}catch (e:Error) {
				
			}

		}
	}
	
}

/*



		
				if(component_mc._vcmID){
					cueCardID = "?id="+component_mc._vcmID;
				};
				if(button.service.ServiceCode._value != "email"){
					button.url = "http://www.addthis.com/bookmark.php?pub="+component_mc.ClientCode+"&s="+button.service.ServiceCode._value+"&url="+component_mc.BookMarkURL+cueCardID+"&title="+component_mc.TITLE;
				}
				
				 rootURL = _root.snasapiURL.split("nbcarchive/snas")[0];
		//front_mc.addThis_mc.vcmID = cueCardID+"&amp;bgColor=FF0000";
		//front
		front_mc.addThis_mc._BookMark_Title = "ICUE: "+xml_array.Title;
		front_mc.addThis_mc._XMLSource = rootURL + "files/icue/site/flash/components2/addThis.xml" //"components/addThis.xml";//
		front_mc.addThis_mc.BookMarkURL = rootURL+"portal/site/iCue/chapter?cuecard="+xml_array.Asset_Id;
		
	

*/