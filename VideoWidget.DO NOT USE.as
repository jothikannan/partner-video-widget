﻿package 
{
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.media.Video;
	import flash.system.LoaderContext;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.filters.BevelFilter;

	import flash.net.navigateToURL;
	import flash.utils.SetIntervalTimer;
	
	import flash.system.SecurityDomain;
	import flash.events.FullScreenEvent;
	import fl.motion.easing.Elastic;
	import fl.motion.Tweenables;
	import fl.transitions.Tween;
    import flash.display.*;
    import flash.events.*;
	import flash.geom.Rectangle;
    import flash.net.*;
    import flash.system.*;
	import flash.external.ExternalInterface;
	import flash.text.TextField;
	import flash.text.TextFormat;
	//import com.nbcuni.outlet.extensions.embedded_player.api.EmbeddedPlayerConstants
	//import com.nbcuni.outlet.extensions.video_player.constants.VideoPlayerConstants;
	import flash.events.FullScreenEvent;
	import flash.geom.Point;
	import flash.display.StageDisplayState;
	import flash.text.TextFieldAutoSize;
	import flash.ui.Mouse;
	import flash.system.Security;
	import flash.utils.Timer;
	
	import flash.desktop.Clipboard;
	import flash.desktop.ClipboardFormats;
	
		import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	
		// -----OSMF Variables--------
		
		public var mediaPlayerSprite:MediaPlayerSprite;
		private var mediaFactory:CustomDefaultMediaFactory;
		private var akamaiLoader:URLLoader ;
		private var videoPlayerOSMF:MediaPlayer;
		private var mediaElement:MediaElement;
		private var loadingNewClip:Boolean = true;
		private var akamaiMediaResource:URLResource;
		private var allowHD:Boolean = true;
		
		// Omniture tracking variables
		private var lastRecordedTime:Number = 0;
		private var canRemoveLoadingMessage:Boolean = true;
		public var video_helper:StageVideoHelper;
		
		
		
		
		// Content
		private var HDN_MULTI_BITRATE_VOD:String;
		// Plugins
		private static const AKAMAI_PLUGIN:String =	"AkamaiAdvancedStreamingPlugin2.7_osmf1.6.swf"; // "AkamaiAdvancedStreamingPlugin2.7.6_osmf1.5.swf"; //  "AkamaiAdvancedStreamingPlugin.swf"; 
	
	
	
	/**
	 * ...
	 * @author Ari Oshinsky
	 */
	public class  VideoWidget extends MovieClip {
		
		private var isPlaying:Boolean = true;
		
	// Outlet Variables
		private var loader:Loader;
		private var _playerComponent; // The outlet framework
		private var embeddedPlayer; // The outlet embedded Player
		private var videoPlayer; // the video Player itselfe
		private var contentMetadata; //contentMetadataDAOID
		private var metaData_obj:Object; // actual object with the meta data
		//private var omniture:Object = omni_mc;
		
		private var overridesSet:Boolean = false;
	
		public var videoHasEnded:Boolean = false;
		private var omnitureExtension:Object;
		private var fullScreen:Boolean = false;
		public var initilized:Boolean = false;
		private var progressTimer:Timer;
		
		private var fadeOutTimer:Timer;
		private var fadeOutTween:Tween;
		private var fadeInTween:Tween;
		private var shareTween:Tween;
	
		private var controlsVisible:Boolean = true;
		private var seekDelayTimer:Timer;
		private var vidX:Number;
		private var vidY:Number;
		private var controlsSize_obj:Object;
	
		private var scrubbing:Boolean = false;
		public var clipID:String;
		var omniture:VideoWidgetOmniture;
		private var bevel:BevelFilter;
		private var copy_btn:MovieClip;
		private var copying:Boolean = false;
		private var videoHasStarted:Boolean = false;

		
		public function VideoWidget() {
			trace("~ NBC Learn Video Widget v.1.5");
			
			Security.allowDomain("*");
			
			
			copy_btn = share_mc.copy_btn;
			setUpShareDrawer();
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			controlsSize_obj = new Object();
			
				splash_mc.visible = false;
				splash_mc.addEventListener(MouseEvent.CLICK, gotoNBCLearn);
				splash_mc.buttonMode = true;
				splash_mc.useHandCursor = true;
				
				
				controls_mc.visible = false;
			
			
			
			
			loader.load(request, loaderContext);
			//loader.alpha = 0;
			fadeOutTimer = new Timer(3000, 1);
			fadeOutTimer.addEventListener(TimerEvent.TIMER_COMPLETE , fadeOutVideoControls);
			bevel = new BevelFilter(-1.3,45,0x999999,.52,0x000000,.74,2,2,1,3);
			omniture = new VideoWidgetOmniture();
			
			
			
				loadingText_mc.x = this.stage.stageWidth / 2 - loadingText_mc.width / 2;
				share_mc.x = this.stage.stageWidth / 2 - share_mc.width / 2;
				setUpOSMF();
			
		}
		// -----------  OSMF AKAMAI VIDEO LOADER ---------------------
	
	private function setUpOSMF() {
		
		
			
			mediaPlayerSprite = new MediaPlayerSprite();
			
			//cueCard.front_mc.content_mc.vid.addChild(mediaPlayerSprite);
			mediaPlayerSprite.x = 0; 
			mediaPlayerSprite.y = 0; 
			
			
			mediaPlayerSprite.scaleMode = ScaleMode.LETTERBOX;
		
			mediaPlayerSprite.width = cueCard.front_mc.content_mc.vid.width;
			mediaPlayerSprite.height = cueCard.front_mc.content_mc.vid.height;
		
			
		
			mediaPlayerSprite.mediaPlayer.addEventListener(	MediaErrorEvent.MEDIA_ERROR, onMediaError);
			mediaPlayerSprite.mediaPlayer.addEventListener(TimeEvent.COMPLETE, _endPlaying);
			mediaPlayerSprite.mediaPlayer.addEventListener(TimeEvent.CURRENT_TIME_CHANGE, timeUpdate);
			mediaPlayerSprite.mediaPlayer.addEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, State_Change_OSMF) 
		
			videoPlayerOSMF = mediaPlayerSprite.mediaPlayer;
	
			mediaFactory = new CustomDefaultMediaFactory();
	
			loadPlugin(cueCard.folderDirectory + AKAMAI_PLUGIN);
			
			
			
			//getAkamaiToken("http://preview-archives.nbclearn.com/portal/testjsp/smil.jsp?assetID=b739125d032c31681b609c13aa1a5c42");
		
	}
	
	
	public function loadPlugin(source:String):void{
		
				var resource:URLResource = new URLResource(source);
		
		
			
				pluginSetupListeners();
				mediaFactory.loadPlugin(resource);
				
			
	}
			
		private function pluginSetupListeners(add:Boolean=true):void{
			
			if (add){
				mediaFactory.addEventListener(MediaFactoryEvent.PLUGIN_LOAD, onPluginLoad);
				
				mediaFactory.addEventListener(MediaFactoryEvent.PLUGIN_LOAD_ERROR, onPluginLoadError);
				
			}else{
				mediaFactory.removeEventListener(MediaFactoryEvent.PLUGIN_LOAD,	onPluginLoad);
				mediaFactory.removeEventListener(MediaFactoryEvent.PLUGIN_LOAD_ERROR,onPluginLoadError);
			}
		}
	
		
		
		function onPluginLoad(event:MediaFactoryEvent):void
		{
			initilized = true;
			trace("~Akamai plugin loaded successfully.");
			initControlEvents();
			pluginSetupListeners(false);
		
			cueCard.componentReady("video");
			captions = new Captions(cueCard.front_mc.content_mc.captions_mc, videoPlayerOSMF, controls_mc.captions_btn, cueCard.always_mc.mainDragBox);
			cueCard.stage.addEventListener(FullScreenEvent.FULL_SCREEN, _fullScreenEvent);  // full screen event tracking
			
			
			
			Log.loggerFactory = new AkamaiConsoleLoggerFactory(); // Dissable Logging and trace statments from the akamai plugin
			//AkamaiConsoleLogger.stopConsoleListening();  // additional methods.
			
			var black_bg:MovieClip = cueCard.front_mc.content_mc.blackBackround;
			
			video_helper = new StageVideoHelper(this.stage, new Rectangle(vid.x, vid.y, vid.width, vid.height));
		
			trace("~ Browser: " + cueCard.browser);
			trace("~ OS: "  + cueCard.OS);
			

			
			
			
		}
		
	
		
		private function init(e:Event = null) {
			trace("~frameworkInit");
			_playerComponent = loader.content;
			_playerComponent.addEventListener("framework_init_success", onOutletInitComplete);
			_playerComponent.init("http://icue.nbcunifiles.com/icue/files/nbclearn/site/video/widget/icue_300x250_outlet_config.xml"); //  http://video.nbcuni.com/PlayerConfig/nbc/outlet_config.xml");  //icue_300x250_outlet_config.xml"); //  //http://video.nbcuni.com/PlayerConfig/nbcnews/icue_445x250_outlet_config.xml );
// PRODUCTION http://icue.nbcunifiles.com/icue/files/nbclearn/site/video/widget/
		}
		private function contentError(event:Object) {
			
			trace("~ContentError Load Error");
			
			
		}
		private function geoPathError(event:Object) {
			trace("~------------------------")
			trace("~GeoPathError");
			trace(event);
			trace("~------------------------")
		}
		
		private function onPlayerStateChange(e:Object):void {
			
		
			
			
			try{
				if (e.data.state == "EmbeddedPlayer.STATE_READY" && overridesSet == false) {
					overridesSet = true;
					
				}
			}catch(e:Error) {
				trace("~ERROR: Could not set omniture overides");
				trace("~"+e);
			}	
		}	
		
		private function onOutletInitComplete(e:Event = null):void {
			
			//trace("~Trying to INIT");
			
			embeddedPlayer = _playerComponent.getOutletExtension("appStarter"); 
			videoPlayer = _playerComponent.getOutletExtension("player"); 
			omnitureExtension = _playerComponent.getOutletExtension("metricManager");
			
			
			contentMetadata = _playerComponent.getOutletExtension("contentMetadataDAOID");
			
			contentMetadata.addEventListener("CONTENT_METADATA.clip_info_update", metaDataReceived);
			
			contentMetadata.addEventListener("ContentError.GEO_PATH_ERROR", geoPathError);
			contentMetadata.addEventListener("ContentError.EVENT_TYPE", contentError);
			
			embeddedPlayer.addEventListener("StateUpdateEvent.UPDATE", onPlayerStateChange);
			
		
			try {
				
				embeddedPlayer.autoPlay = false;
				
			}catch(e:Error) {
				trace("~failed embeddedPlayer.autoPlay");
			}	
			
			playerEvents();
			initControlEvents();
		
			
			initilized = true;
			trace("~onOutletInitComplete");
			//embeddedPlayer.changeControls(" ");
			if(this.stage.loaderInfo.parameters.VIDEO_ID){
				clipID = this.stage.loaderInfo.parameters.VIDEO_ID;
			}else {
				trace("~ Could Not load CLip");
				//clipID = "1361781";
			}
			share_btn.visible = true;
			//var delayTimer:Timer = new Timer(2000, 1);
			//delayTimer.addEventListener(TimerEvent.TIMER_COMPLETE, function(e:TimerEvent) {
				trace("~Play Video Now");
				trace("clipID: " + clipID);
				embeddedPlayer.playVideo(clipID, false);
			//});
			//delayTimer.start();
			
		}
		private function metaDataReceived(info) {
			embeddedPlayer.resize(this.stage.stageWidth, this.stage.stageHeight);
			loadingText_mc.visible = false;
			try{
				metaData_obj = embeddedPlayer.getMetaData().properties;
				
			
			var _title:String;
			var _collection:String = metaData_obj["show"];
			if (metaData_obj["title"] != "" && metaData_obj["title"] != " ") {
				_title = metaData_obj["title"];
			}else {
				_title = metaData_obj["headline"];
			}
			
			
			omniture.overrideVideoPlayer(omnitureExtension, embeddedPlayer, videoPlayer,_title, _collection);
					
					omniture.setUpOmniture(_title,_collection);
			for( var i in metaData_obj) {
				//trace("~ " + i + ":  " + metaData_obj[i]);
				
			}
			}catch (e:Error) {
					trace("could not get metadata");
			}
			
		}
		
	
		
		public function loadClip(_clip) {
			if(initilized){
		
				loader.alpha = 0;
						
		
				//embeddedPlayer.showMessage("LOADING....");
				//playClip(String(_clip));
			}
				
		}
	
		
		
		private function playerEvents() {
			
			//EMBEDDED PLAYER
			embeddedPlayer.addEventListener("Player.start", _startPlaying);
			embeddedPlayer.addEventListener("Player.end", _endPlaying);
			videoPlayer.addEventListener("Player.seekFinished", _seekFinished);
			embeddedPlayer.addEventListener("Player.buffering", _buffering);
			
			this.stage.addEventListener(FullScreenEvent.FULL_SCREEN, _fullScreenEvent);
			// VIDEO PLAYER
			videoPlayer.addEventListener("Player.paused", _pause);	
			videoPlayer.addEventListener("Player.playing", _play);	
			videoPlayer.addEventListener("Player.volumeMute", _mute);
			videoPlayer.addEventListener("Player.error", loadError);
			
			
		}
		private function loadError(e:Event) {
			trace("---LOAD ERROR---");
			if (videoHasStarted == true) {
				trace("~ Goto End Screen");
				_endPlaying();
			}
		}
		
		private function _buffering(e:Event) {
			//trace("~ buffering");
			loadingText_mc.visible = true;

		}
		private function _startPlaying(e:Event) {
			
		
			loadingText_mc.visible = false;
			controls_mc.visible = true;
			videoHasEnded = false;
			isPlaying = true;
			videoHasStarted = true;
			loader.alpha = 1;
			embeddedPlayer.resize(this.stage.stageWidth, this.stage.stageHeight);
			updatePlayState();
			try{
				omniture.videoStart(metaData_obj["title"], metaData_obj["show"]);
			}catch (e:Error) {
				omniture.videoStart("unknown", "unknown");
			}
			
			

		}
		private function _endPlaying(e:Event = null){
			
			
			isPlaying = false;
			videoHasEnded = true;
			
			videoPlayer.visible = false;
			if (fullScreen == true) {
				goFullScreen();
			}
			
		
			
			splash_mc.x = this.stage.stageWidth / 2 - splash_mc.width / 2;
			splash_mc.y = this.stage.stageHeight / 2 - splash_mc.height / 2;
			splash_mc.visible = true;
			vid.visible = false;
		
			//omniture.trackEvent("event29", "Video End");
			
			
			
			
		}
		
		private function _seekFinished(e:Event) {
				var currentTime:Number = embeddedPlayer.getCurrentTimeStart();
				var totalTime:Number = embeddedPlayer.getTotalTime();
				//trace("~_seekFinished");
				//trace("~currentTime: " + Math.floor(currentTime));
				//trace("~totalTime: " + Math.floor(totalTime));
				if (Math.floor(currentTime) >= Math.ceil(totalTime)) {
					_endPlaying();
				}
		}
		
		
		private function gotoNBCLearn(e:MouseEvent) {
			
			var nbclearnRequest:URLRequest = new URLRequest("http://www.nbclearn.com");
			navigateToURL(nbclearnRequest, "_blank");
			var trackNbcLearnTimer:Timer = new Timer(2000, 1);
			trackNbcLearnTimer.addEventListener(TimerEvent.TIMER_COMPLETE, trackGotoNBCLearn);
			trackNbcLearnTimer.start();
			
		}
		private function trackGotoNBCLearn(e:Object = null) {
			omniture.trackGotoNBCLearn();
		}
			
		private function _play(e:Event){
			//trace("~ PLAYER PLAY");
			isPlaying = true;
			videoHasEnded = false;
			updatePlayState();
		}
		private function _pause(e:Event){
	
			//trace("~ PLAYER IS PAUSED");
			isPlaying = false;
			updatePlayState();
		}
		
		private function _mute(e:Event) {
			//trace("~_mute");
		}
		private function muteUnMute() {

			var volume:Number = embeddedPlayer.getVolume();
			
			if (volume != 0) {
				controls_mc.mute_btn.gotoAndStop("unmute");
			}else {
				controls_mc.mute_btn.gotoAndStop("mute");
			}
		}

		private function initControlEvents(event:Event = null) {
			//trace("~INIT VIDEO EVENTS 2.0");
			//embeddedPlayer.resize(this.stage.stageWidth, this.stage.stageHeight);
			controls_mc.x = this.stage.stageWidth / 2 - controls_mc.width / 2;
			controls_mc.y = this.stage.stageHeight  - (controls_mc.height +10);
		// Click Events
			controls_mc.play_btn.addEventListener(MouseEvent.CLICK,playHandler);
			controls_mc.mute_btn.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
				embeddedPlayer.mute();
				muteUnMute();
			});
			controls_mc.fullScreen_btn.addEventListener(MouseEvent.CLICK, goFullScreen);
			controls_mc.progressBar_mc.addEventListener(MouseEvent.CLICK, progressBarClickHandler);
			controls_mc.volume_mc.hotspot.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
				var mousePossition:Number = controls_mc.volume_mc.hotspot.mouseX; 
				var nextPossition:Number = (Math.floor(mousePossition / 3) * 3) + 3;
			
				
				controls_mc.volume_mc.fullness_mc.fill_mc.width = nextPossition;
				nextPossition *= 3;
				if (nextPossition > 100) {
					nextPossition = 100;
				}
				
				var volume:Number = embeddedPlayer.getVolume();
				if (volume == 0) {
					embeddedPlayer.mute();
					muteUnMute();
				}
				embeddedPlayer.setVolume(nextPossition);
				
			});
		
			
		// Roll Over Events
			// Fade
			this.stage.addEventListener(MouseEvent.MOUSE_MOVE, fadeInVideoControls);
			
		// Controls Buttons Roll Over Events
			// Roll Over
			controls_mc.play_btn.addEventListener(MouseEvent.ROLL_OVER, hiLightControl);
			controls_mc.mute_btn.addEventListener(MouseEvent.ROLL_OVER,  hiLightControl);
			controls_mc.fullScreen_btn.addEventListener(MouseEvent.ROLL_OVER, hiLightControl);
			controls_mc.scrubBar.addEventListener(MouseEvent.ROLL_OVER, hiLightControl);
			
			controls_mc.volume_mc.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				Colors.changeTint(controls_mc.volume_mc,0xff6600,.5)
			});
			// Roll Out
			controls_mc.mute_btn.addEventListener(MouseEvent.ROLL_OUT, unHiLightControl);
			controls_mc.fullScreen_btn.addEventListener(MouseEvent.ROLL_OUT, unHiLightControl);
			controls_mc.scrubBar.addEventListener(MouseEvent.ROLL_OUT, unHiLightControl);
			controls_mc.play_btn.addEventListener(MouseEvent.ROLL_OUT, unHiLightControl);
			controls_mc.volume_mc.addEventListener(MouseEvent.ROLL_OUT, unHiLightControl);
		
		
			progressTimer = new Timer(500);
			progressTimer.addEventListener(TimerEvent.TIMER, updateProgressBar);
			progressTimer.start();
		// Button mode controls
			controls_mc.scrubBar.buttonMode = true;
			controls_mc.scrubBar.useHandCursor = true;
			controls_mc.play_btn.buttonMode = true;
			controls_mc.play_btn.useHandCursor = true;
			controls_mc.mute_btn.buttonMode = true;
			controls_mc.mute_btn.useHandCursor = true;
			controls_mc.fullScreen_btn.buttonMode = true;
			controls_mc.fullScreen_btn.useHandCursor = true;
		
	
			controls_mc.volume_mc.hotspot.buttonMode = true;
			controls_mc.volume_mc.hotspot.useHandCursor = true;
			
			
		// Scrubbing Event
			
			controls_mc.scrubBar.addEventListener(MouseEvent.MOUSE_DOWN, startScrubbing);
			
			seekDelayTimer = new Timer(600, 3);
			seekDelayTimer.addEventListener(TimerEvent.TIMER_COMPLETE, function() {
				try{
					updateProgressBar();
				}catch (e:Error) {
					trace("SEEK ERROR");
				}
			});
		}
		
	
		
		public function fadeOutVideoControls(object:Object = null):void {
			if(fadeOutTween){
				fadeOutTween.stop();
			}
			if(fadeInTween){
				fadeInTween.stop();
			}
			if (shareTween) {
					shareTween.stop();
			}
			
			fadeOutTween = animateMovieClip(controls_mc, "alpha", 0, 2);
			shareTween = animateMovieClip(share_btn, "alpha", 0, 2);
			Mouse.hide();
			controlsVisible = false;
		}
		
		public function fadeInVideoControls(object:Object = null):void {
			if (!controlsVisible) {
				controlsVisible = true;
				if(fadeOutTween){
					fadeOutTween.stop();
				}
				if(fadeInTween){
					fadeInTween.stop();
				}
				if (shareTween) {
					shareTween.stop();
				}
				
				Mouse.show();
				fadeInTween = animateMovieClip(controls_mc, "alpha", 1, .5);
				shareTween = animateMovieClip(share_btn, "alpha", 1, .5);
				
			}
			
			
		
			fadeOutTimer.stop();
			fadeOutTimer.reset();
			fadeOutTimer.start();
			
		}
		
		
		
		private function hiLightControl(e:MouseEvent) {
			//Colors.changeTint(e.currentTarget,0x4D40BF,.6)
			Colors.changeTint(e.currentTarget,0xff6600,1)
			
		}
		private function unHiLightControl(e:MouseEvent) {
			Colors.changeTint(e.currentTarget,0xFFFFFF,.0)
			
		}
		
		private function startScrubbing(e:MouseEvent) {
			this.stage.addEventListener(MouseEvent.MOUSE_UP, stopScrubbing);
			this.stage.addEventListener(MouseEvent.MOUSE_MOVE, scrubBarIsMoving);
			scrubbing = true;
			var rect:Rectangle = new Rectangle(controls_mc.progressBar_mc.x, controls_mc.scrubBar.y, controls_mc.progressBar_mc.width, 0);
			controls_mc.scrubBar.startDrag(false, rect);
			
		}
		private function stopScrubbing(e:MouseEvent) {
			
			this.stage.removeEventListener(MouseEvent.MOUSE_UP, stopScrubbing);
			this.stage.removeEventListener(MouseEvent.MOUSE_MOVE, scrubBarIsMoving);
			controls_mc.scrubBar.stopDrag();
			var totalTime:Number = embeddedPlayer.getTotalTime();
			var percent:Number = controls_mc.progressBar_mc.elapsed_mc.width / 100
				
			embeddedPlayer.seekTo(totalTime * percent);
			scrubbing = false;
		}
		
		private function scrubBarIsMoving(e:MouseEvent = null) {
			var distance:Number = (controls_mc.scrubBar.x - controls_mc.progressBar_mc.x);
			var percent:Number = distance / controls_mc.progressBar_mc.width;
			
			controls_mc.progressBar_mc.elapsed_mc.width = percent*100;
			var totalTime:Number = embeddedPlayer.getTotalTime();
			updateDisplayTime(totalTime*percent, totalTime);
			
		}
		
		private function goFullScreen(e:MouseEvent = null) {
			// GO FULL SCREEN
			
			//trace("GO FULL SCREEN: " + fullScreen);
			try {
				if (!fullScreen) {
					vidX = vid.x;
					vidY = vid.y;
					var point1:Point = new Point(0, 0);
				//	cueCard.front_mc.content_mc.vid.x = cueCard.front_mc.content_mc.globalToLocal(point1).x;
				//	cueCard.front_mc.content_mc.vid.y = cueCard.front_mc.content_mc.globalToLocal(point1).y ;
					embeddedPlayer.goFullscreen();
				}else {
					
					this.stage.displayState = StageDisplayState.NORMAL;
					
				}
				
			}catch (e:Error) {
				
			}
		}
		
		
			private function _fullScreenEvent(event:FullScreenEvent) {
		
			
			if (event.fullScreen) {
				fullScreen = true; 
			
				
				
				/*
				controlsSize_obj.progressBar_mc = controls_mc.progressBar_mc.width;
				controlsSize_obj.mute_btn = controls_mc.mute_btn.x;
				controlsSize_obj.volume_mc = controls_mc.volume_mc.x;
				controlsSize_obj.fullScreen_btn = controls_mc.fullScreen_btn.x;
				controlsSize_obj.captions_btn = controls_mc.captions_btn.x;
				controlsSize_obj.captions_mc = controls_mc.captions_btn.y;
				controlsSize_obj.captions_mcX = captions.captions_mc.x;
				controlsSize_obj.captions_mcY = captions.captions_mc.y;
				controlsSize_obj.timeDisplay = controls_mc.timeDisplay.x;
				controlsSize_obj.base_mc = controls_mc.base_mc.width;
				controlsSize_obj.x = controls_mc.x;
				controlsSize_obj.y = controls_mc.y;
				
				//controls_mc.scaleX = .74;
				//controls_mc.scaleY = .74;
				resize(cueCard.stage.stageWidth,cueCard.stage.stageHeight-26); // resize the player even more
				controls_mc.y = cueCard.front_mc.content_mc.vid.y + cueCard.front_mc.content_mc.vid.height - 110;
				controls_mc.progressBar_mc.width = cueCard.front_mc.content_mc.vid.width / 2;
				controls_mc.timeDisplay.x = controls_mc.progressBar_mc.x + controls_mc.progressBar_mc.width + 10;
				controls_mc.mute_btn.x = controls_mc.timeDisplay.x + controls_mc.timeDisplay.width;
				controls_mc.volume_mc.x = controls_mc.mute_btn.x + 20;
				controls_mc.fullScreen_btn.x = controls_mc.volume_mc.x + 36;
				controls_mc.captions_btn.x = controls_mc.fullScreen_btn.x + 28;
				controls_mc.base_mc.width = controls_mc.captions_btn.x + 15;
				
				*/
				
				

				//updateProgressBar();
				
				
				controlsSize_obj.xPos = controls_mc.x;
				controlsSize_obj.yPos = controls_mc.y;
				
				//controlsVisibleInFullScreen = true;
			
				
				
				embeddedPlayer.resize(this.stage.stageWidth, this.stage.stageHeight);
				
				controls_mc.scaleX = 2.5;
				controls_mc.scaleY = 2.5;
				
				controls_mc.x = (this.stage.stageWidth / 2) - (controls_mc.width / 2);
				controls_mc.y = this.stage.stageHeight  - (controls_mc.height +10);
				omniture.trackFullScreen();
			
				
			}else {
				
				
				//trace("~_endFullScreen");
				embeddedPlayer.resize(this.stage.stageWidth, this.stage.stageHeight);
				
				
				fullScreen = false;
			
				updateProgressBar();
				Mouse.show();
				fadeInVideoControls();
			
				controls_mc.scaleX = 1;
				controls_mc.scaleY = 1;
				
				controls_mc.x = controlsSize_obj.xPos;
				controls_mc.y = controlsSize_obj.yPos;
			}
		}
		
		
		public function togglePlayPause() {
			try {
				playHandler();
			}catch (e:Error) {
				
			}
		}
		
		private function playHandler(event = null):void {
			
			if (isPlaying) { 
				
				embeddedPlayer.pause();
			
			}else{
				embeddedPlayer.play();
				
			}

		}
		
		private function  updatePlayState(event = null) {
			
			//trace("Video Stage: " + videoPlayer.getState());
			
			if (isPlaying){//outletPlayer._myPlayer.getState() == "playing") {
				controls_mc.play_btn.gotoAndStop("pause");
				
				progressTimer.start();
			}else{
				controls_mc.play_btn.gotoAndStop("play");
				progressTimer.stop();
			}
		}
		private function stopHandler(event:MouseEvent):void {
			
			embeddedPlayer.pause();
			//trace("getState(): " + videoPlayer.getState());
		}
		
		
		
	// JUMP TO A PORTION OF THE VIDEO
		private function progressBarClickHandler(e:MouseEvent) {
			var prog_mc:MovieClip = e.currentTarget as MovieClip;
			var totalTime:Number = embeddedPlayer.getTotalTime();
			var seekTarget:Number = Number(prog_mc.mouseX/100 )*totalTime ;
			//trace("seekTarget: " + seekTarget);
			//trace("videoPlayer.getTotalTime(): " + videoPlayer.getTotalTime());
			embeddedPlayer.seekTo(seekTarget);
			
			seekDelayTimer.stop();
			seekDelayTimer.reset();
			if (isPlaying == false) {
				controls_mc.scrubBar.x = controls_mc.mouseX;
				scrubBarIsMoving();
				seekDelayTimer.start();
			}else {
				updateProgressBar();
			}
			
		}
		
		private function updateDisplayTime(currentTime:Number,totalTime:Number) {
			// Set Time
			var minuets:Number;
			var seconds:Number;
			// Current Time
			minuets = Math.floor(currentTime/60);
			seconds = Math.round(currentTime%60);
			if(seconds == 60){
				seconds = 0;
				minuets += 1;
			}
			controls_mc.timeDisplay.currentTime.text = checkIfValid(minuets) + ":"+checkIfValid(seconds);
			// Total Time
			minuets =  Math.floor(totalTime/60);
			seconds =  Math.round(totalTime%60);
			if(seconds == 60){
				seconds = 0;
				minuets += 1;
			}
			controls_mc.timeDisplay.totalTime.text = checkIfValid(minuets) + ":" + checkIfValid(seconds);
				
		}
		
		private function checkIfValid(testNumber):String{
			if(isNaN(testNumber) || testNumber == undefined || testNumber < 0){ // if the test number is not a number or is undefined
				return "00";
			}else{
				if(testNumber < 10){
					return "0"+testNumber;
				}else{
					return testNumber;
				}
			}
		}
	
		
		
	// MOVE THE PROGRESS BAR AT SET INTERVALS ACCORDING TO THE ELAPSED TIME
		public function updateProgressBar(event:TimerEvent = null) {
	
			if(!scrubbing){
				var currentTime:Number = embeddedPlayer.getCurrentTimeStart();
				var totalTime:Number = embeddedPlayer.getTotalTime();
				var percent:Number = (currentTime / totalTime) * 100;
			
				controls_mc.progressBar_mc.elapsed_mc.width = percent;
				// Scrub Bar
				controls_mc.scrubBar.x = Math.round(controls_mc.progressBar_mc.x + controls_mc.progressBar_mc.width*(percent/100));
				updateDisplayTime(currentTime, totalTime);
			}
			
		}
		
	// DISPLAY THE TIME WHEN MOUSING OVER THE PRGRESS BAR
		private function setUpTimeDisplay() {
			controls_mc.progressBar_mc.addEventListener(MouseEvent.ROLL_OVER, function() {
				
				controls_mc.progressBar_mc.addEventListener(MouseEvent.MOUSE_MOVE, updateTimeDisplay);
				controls_mc.timeBar_mc.visible = true;
				//Resizer.maxFramerate();
			});
			controls_mc.progressBar_mc.addEventListener(MouseEvent.ROLL_OUT, function() {
				controls_mc.timeBar_mc.visible = false;
				controls_mc.progressBar_mc.removeEventListener(MouseEvent.MOUSE_MOVE, updateTimeDisplay);
				//Resizer.idleFramerate();
				
			});
			controls_mc.timeBar_mc.visible = false;
			
		}
		// ANIMATE A SPECIFIC PROPETY OF A MOVIECLIP
		public function animateMovieClip(target_mc:Sprite, property:String ,target:Number,time:Number):Tween {
			var tween:Tween;
			tween = new Tween(target_mc, property, Regular.easeInOut, target_mc[property], target, time, true);
			return tween;
		}
		
		
		private function updateTimeDisplay(e:MouseEvent) {
			var totalTime:Number = embeddedPlayer.getTotalTime();
		// Set Possition
			controls_mc.timeBar_mc.x = controls_mc.mouseX;
			
			// Set Time
			var percent:Number = controls_mc.progressBar_mc.mouseX;
			var currentTime:Number = (percent * totalTime) / 100;
			var time_str:String;
			
			var minuets:Number = 0;
			var seconds:Number;
			var seconds_str:String;
			// Current Time
			minuets = Math.floor(currentTime/60);
			seconds = Math.round(currentTime%60);
			if(seconds == 60){
				seconds = 0;
				minuets += 1;
			}
			if (seconds < 10) {
				seconds_str = "0"+seconds
			}else {
				seconds_str = seconds.toString();
			}
			controls_mc.timeBar_mc.time_txt.text = minuets + ":" + seconds_str;
			
			
		}
		
		private function showShareDrawer(e:MouseEvent = null) {
			
			// Add in code to autodetect width.
		
			if(share_mc.visible == false){
				var share_url:String = '<embed src="http://icue.nbcunifiles.com/icue/files/nbclearn/site/video/widget/NBC_Learn_Video_Widget.swf?VIDEO_ID='+clipID+'" width="300" height="250" style="" allowscriptaccess="always"  salign="tl" quality="high" bgcolor="#000000" name="NBC_Learn_Video" id="NBC_Learn_Video" type="application/x-shockwave-flash" allowfullscreen="true"/>';
				share_mc.url_text.text = share_url;
				share_mc.visible = true;
			}else {
				share_mc.visible = false;
			}
		}
		private function setUpShareDrawer() {
			share_mc.visible = false;
			
			
			// SHARE BUTTON
			share_btn.hit_btn.buttonMode = true;
			share_btn.hit_btn.useHandCursor = true;
			share_btn.hit_btn.addEventListener(MouseEvent.MOUSE_OVER, hiLightControl);
			share_btn.hit_btn.addEventListener(MouseEvent.MOUSE_OUT, unHiLightControl);
			
			share_btn.hit_btn.addEventListener(MouseEvent.CLICK, showShareDrawer);
		
			
			
			
			
		// COPY BTN	
			copy_btn.label_txt.text = "COPY";
			Colors.changeTint(copy_btn.bg, 0x999999, 1);
			
			var this_mc:MovieClip = this;
		
		
		
			copy_btn.buttonMode = true;
			copy_btn.useHandCursor = true;
			
			copy_btn.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
				
				
				Clipboard.generalClipboard.setData(ClipboardFormats.TEXT_FORMAT, share_mc.url_text.text);
				copy_btn.label_txt.text = "COPIED"
				Colors.changeTint(copy_btn.bg, 0x00FF00, 1);
				copying = true;
				
				
				var changeBackTimer:Timer = new Timer(3500, 1);
				changeBackTimer.addEventListener(TimerEvent.TIMER_COMPLETE, changeTextBackToCopy);
				changeBackTimer.start();
				this_mc.stage.focus = share_mc.url_text;
				share_mc.url_text.setSelection(0, share_mc.url_text.length);
				
				omniture.trackShare();
				
				
			});
			copy_btn.addEventListener(MouseEvent.ROLL_OVER, function(e:MouseEvent) {
				if(!copying){
					Colors.changeTint(copy_btn.bg, 0xF36F21, 1);
					copy_btn.filters = [bevel];
				}
			});
			copy_btn.addEventListener(MouseEvent.ROLL_OUT, function(e:MouseEvent) {
				if(!copying){
					Colors.changeTint(copy_btn.bg, 0x999999, 1);
					copy_btn.filters = [];
				}
			});
			
			share_btn.visible = false;
		}
		private function changeTextBackToCopy(e:TimerEvent) {
			copy_btn.label_txt.text = "COPY";
			Colors.changeTint(copy_btn.bg, 0x999999, 1);
			copying = false;
		}
		
	}
	
}