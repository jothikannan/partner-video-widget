﻿package 
{
	
	/**
	 * ...
	 * @author Ari
	 */
	import com.omniture.ActionSource;
	import flash.external.ExternalInterface;
	public class VideoWidgetOmniture 
	{
		private var omni_mc:ActionSource;
	
		private var trackOmnitureEvents:Boolean;
		private var omnitureXML:XMLList;
		public var omnitureExtension:Object;
		public var blackBoardInternalExternal:String;
		public var collectionName:String;
		public var clipName:String;
		
		
		
		public function VideoWidgetOmniture() {
			
			trackOmnitureEvents = true;
			omni_mc = new ActionSource();
		}
	
		public function trackEvent(eventType:String,eventName:String = null) {
			if (trackOmnitureEvents == true) {
				trace("!Track Event: " + eventType);
			// Set Event
				omni_mc.events = eventType;
				omni_mc.prop1 = eventType;
			// TRACK
				omni_mc.trackLink("", 'o', "customEvent");
			// Clear Event
				omni_mc.events = ""; 
				omni_mc.prop1 = "";
			}else {
				trace("! Track Local: " + eventType);
			}
		}
		
		public function setUpOmniture(_clipName:String,_collectionName:String) {
			
			//trace("omnitureXML: " + omnitureXML);
			
		  // OMNITURE INFO FROM THE STATES XML
			omni_mc.trackingServer = "oimg.nbcuni.com";
			omni_mc.account = "nbcuglobal, nbculearnvideowidgets";
			omni_mc.site = "nbclearn.com";
			omni_mc.charSet = "ISO-8859-1";
			omni_mc.currencyCode = "USD";
			omni_mc.visitorNamespace = "nbcuni";
			omni_mc.dc = 122;
			omni_mc.eVar1 = "NBC Learn Mini Widget Player";
			omni_mc.eVar48 = "NBC Learn Mini Widget Player";
			omni_mc.eVar2 = "";
			omni_mc.eVar3 = "";
			omni_mc.events = "";
			omni_mc.products = "";
			omni_mc.prop2 = "";
			omni_mc.pageName = "Video Player";
			 omni_mc.prop8 = "Corporate";
			omni_mc.prop9 = "NBC Learn";
			omni_mc.prop10 = "NBCU";
			omni_mc.eVar45 = omni_mc.prop9;
			omni_mc.eVar6 = _clipName;
			omni_mc.eVar40 = _clipName;
			omni_mc.eVar2 = _collectionName;
			omni_mc.eVar37 = _collectionName;
			
			trace("~_collectionName: " + _collectionName);
			trace("~_clipName: " + _clipName);
			
			
			// SUPER QUIDGET LOCATION
			var quidgetLocation:String;
			//var urlPath = ExternalInterface.call("window.location.href.toString");
			var urlPath
			try{
			 urlPath = ExternalInterface.call('eval', 'document.location.href');
			}catch (e:Error) {
				trace("Location unknown");
			}

			
			if(urlPath == undefined || urlPath == null || urlPath == "undefined" || urlPath == "null" || urlPath == "" || urlPath == " "){
				quidgetLocation= "Location unavailable";
			}else{
				quidgetLocation = urlPath;
			}
			
			omni_mc.eVar7 = quidgetLocation;
		
			/* Turn on and configure ClickMap tracking here */
			omni_mc.trackClickMap = false;
			omni_mc.movieID = "";
			
			/* Turn on and configure debugging here */
			omni_mc.debugTracking = false;
			omni_mc.trackLocal = true;

			/* WARNING: Changing any of the below variables will cause drastic changes
			to how your visitor data is collected.  Changes should only be made
			when instructed to do so by your account manager.*/
			
			
		}
		public function videoStart(_clipName:String, _collectionName:String) {
			
			
			
			omni_mc.events= "event2"
			omni_mc.prop1 = "event2";
			//omni_mc.track()
			omni_mc.trackLink("","o","customEvent");
			
			//Undeclare Values so they will not show up on other calls
			omni_mc.events="";
			omni_mc.prop1 = "";
		
		}

		public function trackGotoNBCLearn(){
	
	
	
			omni_mc.events= "event7"
			omni_mc.prop1 = "event7";
			omni_mc.trackLink("","o","customEvent");
			
			//Undeclare Values so they will not show up on other calls
			omni_mc.events="";
			omni_mc.prop1 = "";

		
		}
		
		public function trackFullScreen() {
			
			omni_mc.events= "event3"
			omni_mc.prop1 = "event3";
			omni_mc.trackLink("","o","customEvent");
			
			//Undeclare Values so they will not show up on other calls
			omni_mc.events="";
			omni_mc.prop1 = "";

		
		}
		public function trackShare() {
			
			omni_mc.events= "event4"
			omni_mc.prop1 = "event4";
			omni_mc.trackLink("","o","customEvent");
			
			//Undeclare Values so they will not show up on other calls
			omni_mc.events="";
			omni_mc.prop1 = "";

		
		}



		public function overrideVideoPlayer(_omnitureExtension:Object,embeddedPlayer:Object,videoPlayer:Object,_clipName:String, _collectionName:String) {
			omnitureExtension = _omnitureExtension
		  
			var omnitureOBJ:Object = new Object();
			omnitureOBJ.omniture_prop8 = "Corporate"; // Division
			omnitureOBJ.omniture_prop9 = "NBC Learn";
			omnitureOBJ.omniture_prop10 = "";
			omnitureOBJ.omniture_prop48 = "NBC Learn Mini Widget Player";
			omnitureOBJ.omniture_prop14 = "NBC Learn";
			omnitureOBJ.omniture_prop11 = "";
			omnitureOBJ.omniture_prop20 = "nbculearnvideowidgets";
			omnitureOBJ.omniture_eVar45 = "NBC Learn";
			omnitureOBJ.omniture_eVar6 = _clipName;
			omnitureOBJ.omniture_eVar2 = _collectionName;
			
			//trace("~_collectionName: " + _collectionName);
			//trace("~_clipName: " + _clipName);	
			
			
			omnitureOBJ.omniture_pageName="";


			
			//trace("-------------------------OMNITURE----------------------------------");
			//trace("omnitureExtension: " + omnitureExtension);
			
			try{
				//omnitureExtension.setOverrides(omnitureOBJ);
				omnitureExtension.updateOverrides(omnitureOBJ);
				//trace("OMNITURE SUCCESS ON omnitureExtension");
			}catch (e:Error) {
				e.message;
				trace("--OMNITURE FAIL ON omnitureExtension--");
				trace(e.message);
			}
			
			//trace("-----------------------------------------------------------");
		}
		
		
		// CHECK FOR UNDEFINED
		private function isNotUndefined(check_obj){
			if(check_obj == undefined || check_obj == "undefined" || check_obj == null || check_obj == "null" || String(check_obj) == "" || String(check_obj) == " "){
				return false;
			}else{
				return true;
			}
		}
		
	}
	
}