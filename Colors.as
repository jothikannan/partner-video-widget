﻿package 
{
	import fl.motion.Color;
	import flash.geom.ColorTransform;
	import flash.geom.Transform;
	
	/**
	 * ...
	 * @author Ari
	 */
	public class Colors extends Color 
	{

		/* 
		 * this allows you to hard change the color of an object 
		 */
		public static function changeColor(image_mc ,color:uint):void {
			
			
			var colorTrans:ColorTransform =  new ColorTransform // set the color
			colorTrans.color = color; // set the color
			var transform:Transform = new Transform(image_mc); // set the color to the background
			transform.colorTransform = colorTrans;
		}
		
		
		/* 
		 * this allows you to shade the color of an object. alphaSet: 1 = full shade, 0 = no color change
		 */
		public static function changeTint(image_mc, color:uint , alphaSet:Number) { /* this allows you to shade the color of an object */
			var cTint:Color = new Color();
			cTint.setTint(color, alphaSet);
			image_mc.transform.colorTransform = cTint;
			
			
		}
		
	}
	
}